var carousel = function () {
    $('.carousel-testimony').owlCarousel({
        loop: true,
        items: 2,
        margin: 30,
        stagePadding: 0,
        nav: true,
        navText: ['<span class="carousel-control-prev-icon">', '<span class="carousel-control-next-icon">'],
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 2,
                nav: true
            },
            1000: {
                items: 2,
                nav: true
            }
        }
    });

};
carousel();